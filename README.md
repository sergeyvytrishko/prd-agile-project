# prd-agile-project #

### Overview ###

Product database application which facilitates CRUD actions as part of Agile Build & Delivery class assessment.

### Getting started ###

* To start run `docker-compose up --build`
* Accessible at http://localhost/

### Built with: ###

- Maven
- Spring Boot
- Thymeleaf
- MySQL
- Nginx
- Docker