package com.products.springboot.controller;

import com.products.springboot.model.ProductModel;
import com.products.springboot.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("productList", productService.getAllProducts());
        return "index";
    }

    @GetMapping("/new-product")
    public String newProduct(Model model) {
        ProductModel product = new ProductModel();
        model.addAttribute("product", product);
        return "product";
    }
    
    @PostMapping("/save-product")
    public String saveProduct(@ModelAttribute("product") ProductModel product) {
        productService.saveProduct(product);
        return "redirect:/";
    }
    
    @GetMapping("/delete-product/{id}")
    public String deleteProduct(@PathVariable(value = "id") long id) {
        productService.deleteProductById(id);
        return "redirect:/";
    }
    
    @GetMapping("/product/{id}")
	public String getProductById(@PathVariable ( value = "id") long id, Model model) {
		ProductModel product = productService.getProductById(id);
		model.addAttribute("product", product);
		return "product";
	}
}
