package com.products.springboot.service;

import com.products.springboot.model.ProductModel;
import com.products.springboot.repostory.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<ProductModel> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public ProductModel saveProduct(ProductModel product) {
        ProductModel savedEntity = productRepository.save(product);
        return savedEntity;
    }

    @Override
    public void deleteProductById(long id) {
        productRepository.delete(id);
    }

    @Override
	public ProductModel getProductById(long id) {
		return productRepository.findOne(id);
	}
}
