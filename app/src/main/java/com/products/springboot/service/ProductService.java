package com.products.springboot.service;

import java.util.List;

import com.products.springboot.model.ProductModel;

public interface ProductService {
    List<ProductModel> getAllProducts();

    ProductModel saveProduct(ProductModel product);

    void deleteProductById(long id);

    ProductModel getProductById(long id);
}
