package com.products.springboot.repostory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.products.springboot.model.ProductModel;

@Repository
public interface ProductRepository extends JpaRepository<ProductModel, Long>{}
