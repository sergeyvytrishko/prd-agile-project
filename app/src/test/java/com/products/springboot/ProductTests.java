package com.products.springboot;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;

import com.products.springboot.model.ProductModel;
import com.products.springboot.repostory.ProductRepository;
import com.products.springboot.service.ProductServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProductTests {
	@Mock
	private ProductRepository mockRepository;

	@InjectMocks
	private ProductServiceImpl service;

	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getProductList() {
		Mockito.when(mockRepository.findAll()).thenReturn(Arrays.asList(new ProductModel()));

		List<ProductModel> products = service.getAllProducts();

		assertFalse(products.isEmpty());
		assertEquals(1, products.size());
		verify(mockRepository, times(1)).findAll();
	}
	
	@Test
	public void saveProduct() {
		ProductModel newPoduct = new ProductModel("name", 1, "description");

		Mockito.when(mockRepository.save(newPoduct)).thenReturn(newPoduct);

		ProductModel product = service.saveProduct(newPoduct);

		assertEquals(newPoduct.getName(), product.getName());
		verify(mockRepository, times(1)).save(newPoduct);
	}
	
	@Test
	public void deleteProduct() {
		service.deleteProductById(1);

		verify(mockRepository, times(1)).delete(new Long(1));
	}
	
	@Test
	public void getProductById() {
		ProductModel preparedProduct = new ProductModel();
		long id = 1;

		Mockito.when(mockRepository.findOne(id)).thenReturn(preparedProduct);
		
		ProductModel product = service.getProductById(id);
		
		verify(mockRepository, times(1)).findOne(id);
		assertEquals(preparedProduct.getId(), product.getId());
	}
}
